#!/usr/bin/env sh

docker run -d --name devpi-server -p 127.0.0.1:3141:3141 -v $PWD/serverdir:/data/devpi -v $PWD/.htaccess:/root/.htaccess -e ROOT_PASSWORD=$ROOT_PASSWORD -e NO_SELF_REGISTER=true -e OUTSIDE_URL=http://localhost:3141 garymonson/devpi-server:4.3.2-1
