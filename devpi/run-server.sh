#!/usr/bin/env sh

set -e

export SERVERDIR=/data/devpi
[[ -f $SERVERDIR/.serverversion ]] || first_time="true"

ARGS=
if [[ ! -z "$NO_SELF_REGISTER" ]]; then
    echo "Disabling self-registration"
    ARGS="$ARGS --restrict-modify root"
else
    echo "Self-registration left available"
fi

if [[ ! -z "$first_time" ]]; then
    echo "Changing root password"

    devpi-server --host 127.0.0.1 --port 3140 --start --serverdir $SERVERDIR --init $ARGS

    devpi use http://127.0.0.1:3140
    devpi login root --password=''
    devpi user -m root password="$ROOT_PASSWORD"

    echo "Creating public index under root"
    devpi index -y -c public pypi_whitelist='*'

    devpi-server --serverdir $SERVERDIR --stop
fi

if [[ ! -z "$OUTSIDE_URL" ]]; then
    ARGS="$ARGS --outside-url $OUTSIDE_URL"
fi

devpi-server --host 127.0.0.1 --port 3140 --start --serverdir $SERVERDIR $ARGS

cp /root/.htaccess /etc/nginx/.htaccess
chmod 600 /etc/nginx/.htaccess
chown nginx:nginx /etc/nginx/.htaccess
nginx -c /etc/nginx/nginx.conf

tail -f $SERVERDIR/.xproc/devpi-server/xprocess.log
