# Devpi-server

Devpi-server with nginx in front to provide basic auth protection.

## Building

```
docker build -t garymonson/devpi-server:4.3.2-1 .
```

## Running

Ensure you have a .htaccess file to configure access.  Then:

```
docker run -d --name devpi-server -p 127.0.0.1:3141:3141 -v $PWD/serverdir:/data/devpi -v $PWD/.htaccess:/root/.htaccess -e ROOT_PASSWORD=$ROOT_PASSWORD -e NO_SELF_REGISTER=true -e OUTSIDE_URL=https://devpi.example.com garymonson/devpi-server:4.3.2-1
```

The above example assumes you have an SSL-terminating proxy at https://devpi.example.com standing in front of your devpi container and forwarding to port 3141 in the container.  The OUTSIDE_URL parameter tells devpi to make all links use that URL, so they work correctly.  Set this according to your setup.

$ROOT_PASSWORD env var is used to set the root password when the container is started the first time.

You can use SHA-512 hashed passwords in your .htaccess:

```
echo -n "myname:" >> .htaccess
mkpasswd -m sha-512 >> .htaccess
```

Please note that on some systems, the above command will include the "Password:" prompt in the redirected output instead of printing to screen, so you may have to generate the file differently.
